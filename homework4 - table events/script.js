let table = document.createElement('table');
table.classList.add('table');
document.body.appendChild(table);
for (let i = 0; i < 30; i++){
    let row = document.createElement('tr')
    table.appendChild(row);
    for (let j = 0; j < 30; j++) {
        let cell = document.createElement('td');
        cell.classList.add('cell')
        row.appendChild(cell)
    }
}

document.body.addEventListener('click', () => {
    if (event.target == document.body) {
        table.classList.toggle('black');
    } else if (event.target.classList.contains('cell')) {
        event.target.classList.toggle('black');
    }
})