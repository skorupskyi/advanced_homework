const SIZE_SMALL = {
    kind: 'small',
    price: 50,
    calories: 20
}
const SIZE_LARGE = {
    kind: 'big',
    price: 100,
    calories: 40
}
const STUFFING_CHEESE = {
    kind: 'cheese',
    price: 10,
    calories: 20
}
const STUFFING_SALAD = {
    kind: 'salad',
    price: 20,
    calories: 5
}
const STUFFING_POTATO = {
    kind: 'potato',
    price: 15,
    calories: 10
}
const TOPPING_MAYO = {
    kind: 'mayonese',
    price: 20,
    calories: 5
}
const TOPPING_SPICE = {
    kind: 'spice',
    price: 15,
    calories: 0
}

class HamburgerExeption extends Error {
    constructor(message) {
        super(message)
        this.name = "HamburgerExeption:";
        this.message = message;
    }
}
class Hamburger {
    constructor(size, stuffing) {
        if (!size || !stuffing) {
            throw new HamburgerExeption('No size/stuffing given')
        }
        if (size != SIZE_SMALL && size != SIZE_LARGE) {
            throw new HamburgerExeption('Invalid size')
        }

        this.size = Object.assign({}, size);
        this.stuffing = Object.assign({}, stuffing);
        this.toppingsArr = [];
    }

    set size(newSize) {
        return this._size = Object.assign({}, newSize)
    }
    get size() {
        return this._size
    }
    set stuffing(newStuffing) {
        return this._stuffing = Object.assign({}, newStuffing)
    }
    get stuffing() {
        return this._stuffing
    }

    addTopping(topping) {
        try {

            for (let elem of this.toppingsArr) {
                if (elem.kind == topping.kind) {
                    throw new HamburgerExeption('Duplicate topping');
                }
            }

            this.toppingsArr.push(Object.assign({}, topping));
        } catch (err) {

            if (err instanceof HamburgerExeption) {
                console.log(`${err.name} ${err.message}`)
            } else {
                throw err
            }

        }
    }


    removeTopping(topping) {
        try {

            if (this.toppingsArr.length == 0) {
                throw new HamburgerExeption('Missing topping');
            }
            for (let elem of this.toppingsArr) {
                if (elem.kind == topping.kind) {
                    this.toppingsArr.splice(this.toppingsArr.indexOf(elem), 1);
                }
                else if (this.toppingsArr.indexOf(elem) == this.toppingsArr.length - 1) {
                    throw new HamburgerExeption('Missing topping');
                }
            }

        } catch (err) {

            if (err instanceof HamburgerExeption) {
                console.log(`${err.name} ${err.message}`)
            } else {
                throw err
            }
            
        }
    }

    getToppings() {
        return this.toppingsArr;
    }

    getSize() {
        return this.size.kind;
    }

    getStuffing() {
        return this.stuffing.kind;
    }

    calculatePrice() {
        let price = 0;
        return function findPrice(obj) {
            for (let elem in obj) {
                if (typeof (obj[elem]) == 'object') {
                    findPrice(obj[elem])
                } else if (elem == 'price') {
                    price += obj[elem]
                }
            }
            return price;
        }(this)
    }

    calculateCalories() {
        let calories = 0;
        return function findPrice(obj) {
            for (let elem in obj) {
                if (typeof (obj[elem]) == 'object') {
                    findPrice(obj[elem])
                } else if (elem == 'calories') {
                    calories += obj[elem]
                }
            }
            return calories;
        }(this)
    }
}


// let ham = new Hamburger(SIZE_SMALL, STUFFING_CHEESE)
// ham.addTopping(TOPPING_MAYO); 
// ham.addTopping(TOPPING_SPICE);
// ham.removeTopping(TOPPING_SPICE);
// ham.removeTopping(Hamburger.TOPPING_MAYO);
// console.log(ham.getToppings())
// console.log(ham.getSize())
// console.log(ham.calculatePrice())
// console.log(ham.calculateCalories())
// console.log(ham);

