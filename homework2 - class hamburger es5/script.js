
function HamburgerExeption(message) {
    Error.call(this, message);
    this.name = "HamburgerExeption: ";
    this.message = message;
}
HamburgerExeption.prototype = Object.create(Error.prototype);

function Hamburger(size, stuffing) {
    if (!size || !stuffing) {
        throw new HamburgerExeption('No size/stuffing given')
    }
    if (size != Hamburger.SIZE_SMALL && size != Hamburger.SIZE_LARGE) {
        throw new HamburgerExeption('Invalid size')
    }
    this.size = Object.assign({}, size);
    this.stuffing = Object.assign({}, stuffing);
    this.toppingsArr = [];
}

Hamburger.SIZE_SMALL = {
    kind: 'small',
    price: 50,
    calories: 20
}
Hamburger.SIZE_LARGE = {
    kind: 'big',
    price: 100,
    calories: 40
}
Hamburger.STUFFING_CHEESE = {
    kind: 'cheese',
    price: 10,
    calories: 20
}
Hamburger.STUFFING_SALAD = {
    kind: 'salad',
    price: 20,
    calories: 5
}
Hamburger.STUFFING_POTATO = {
    kind: 'potato',
    price: 15,
    calories: 10
}
Hamburger.TOPPING_MAYO = {
    kind: 'mayonese',
    price: 20,
    calories: 5
}
Hamburger.TOPPING_SPICE = {
    kind: 'spice',
    price: 15,
    calories: 0
}

Hamburger.prototype.addTopping = function (topping) {

    for (elem of this.toppingsArr) {
        if (elem.kind == topping.kind) {
            throw new HamburgerExeption('Duplicate topping');
        }
    }

    this.toppingsArr.push(Object.assign({}, topping));
}

Hamburger.prototype.removeTopping = function (topping) {

    if (this.toppingsArr.length == 0) {
        throw new HamburgerExeption('Missing topping');
    }

    for (elem of this.toppingsArr) {
        if (elem.kind == topping.kind) {
            this.toppingsArr.splice(this.toppingsArr.indexOf(elem), 1);
        }
        else if (this.toppingsArr.indexOf(elem) == this.toppingsArr.length - 1) {
            throw new HamburgerExeption('Missing topping');
        }
    }

}

Hamburger.prototype.getToppings = function () {
    return this.toppingsArr;
}

Hamburger.prototype.getSize = function () {
    return this.size.kind;
}

Hamburger.prototype.getStuffing = function () {
    return this.stuffing.kind;
}

Hamburger.prototype.calculatePrice = function () {
    var price = 0;
    return function findPrice(obj) {
        for (elem in obj) {
            if (typeof (obj[elem]) == 'object') {
                findPrice(obj[elem])
            } else if (elem == 'price') {
                price += obj[elem]
            }
        }
        return price;
    }(this)
}

Hamburger.prototype.calculateCalories = function () {
    var calories = 0;
    return function findPrice(obj) {
        for (elem in obj) {
            if (typeof (obj[elem]) == 'object') {
                findPrice(obj[elem])
            } else if (elem == 'calories') {
                calories += obj[elem]
            }
        }
        return calories;
    }(this)
}



var ham = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE)

//ham.addTopping(Hamburger.TOPPING_MAYO); 
// ham.addTopping(Hamburger.TOPPING_SPICE);
// ham.removeTopping(Hamburger.TOPPING_SPICE);
// ham.removeTopping(Hamburger.TOPPING_MAYO);
// ham.getToppings()
// ham.getSize()
// console.log(ham.calculatePrice())
// console.log(ham.calculateCalories())
// console.log(ham);











